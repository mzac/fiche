FROM alpine:latest

RUN apk add --update --no-cache bash build-base git tzdata

RUN mkdir /fiche
RUN mkdir /fiche-data

RUN git clone https://github.com/solusipse/fiche.git /fiche

RUN cd /fiche; make

RUN chown 1001:1001 /fiche
RUN chown 1001:1001 /fiche-data

RUN chmod a+w /fiche
RUN chmod a+w /fiche-data

ADD run.sh /opt/run.sh
RUN chmod a+x /opt/run.sh

VOLUME ["/fiche-data"]

EXPOSE 9999

USER 1001

CMD ["/opt/run.sh"]
