#!/bin/bash -e

# Dockerfile Variables
export FICHE_DOMAIN
export FICHE_SLUG_SIZE
export FICHE_HTTPS

# Check for domain variable
if [ -z "$FICHE_DOMAIN" ]; then
	OPTION_DOMAIN="docker.example.com"
else
	OPTION_DOMAIN=$FICHE_DOMAIN
fi

# Check for slug size variable
if [ -z "$FICHE_SLUG_SIZE" ]; then
	OPTION_SLUG_SIZE="4"
else
	OPTION_SLUG_SIZE=$FICHE_SLUG_SIZE
fi

# Check for HTTPS variable
if [ -z "$FICHE_HTTPS" ]; then
	OPTION_HTTPS=""
else
	OPTION_HTTPS="-S"
fi

/fiche/fiche -d $OPTION_DOMAIN -s $OPTION_SLUG_SIZE -o /fiche-data $OPTION_HTTPS